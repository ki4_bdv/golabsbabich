package main

import (
	"fmt"
	"math/rand"
	"os"
	"sync"
	"time"
)

type Bank struct {
	Name      string
	bankMoney float64
	Deposit   float64
	Credit    float64
	Clients   []*Client
}
type Client struct {
	idBank        int
	Name          string
	Surname       string
	AccountNumber int
	cDeposit      float64
	cCredit       float64
}

//Global Banks
var banks []*Bank

func newBank(name string, bankMoney, deposit, credit float64, clients []*Client) *Bank {
	nB := Bank{name, bankMoney, deposit, credit, clients}
	return &nB
}

func newClient(idBank int, name, surname string, accountNumber int, cDeposit, cCredit float64) *Client {
	c := Client{idBank, name, surname, accountNumber, cDeposit, cCredit}
	return &c
}

func (b Bank) getName() string {
	return b.Name
}

func (b Bank) getBankMoney() float64 {
	return b.bankMoney
}

func (b Bank) getDeposit() float64 {
	return b.Deposit
}
func (b Bank) getCredit() float64 {
	return b.Credit
}

func (b Bank) getClients() []*Client {
	return b.Clients
}

func (c Client) getName() string {
	return c.Name
}

func (c Client) getSurname() string {
	return c.Surname
}

func (c Client) getAccountNumber() int {
	return c.AccountNumber
}

func (c Client) getcDeposit() float64 {
	return c.cDeposit
}

func (c Client) getcCredit() float64 {
	return c.cCredit
}

func (b *Bank) setBankName(name string) {
	b.Name = name
}
func (b *Bank) addClient(c Client) {
	b.Clients = append(b.Clients, &c)
}

func (b *Bank) setBankMoney(bankmoney float64) {
	b.bankMoney = bankmoney
}

func (b *Bank) setBankDeposit(deposit float64) {
	b.Deposit = deposit
}

func (b *Bank) setBankCredit(credit float64) {
	b.Credit = credit
}

func (b *Bank) setBankClients(c []*Client) {
	b.Clients = c
}

func (b *Bank) putMoneyOnDeposit(depositMoney float64) {
	b.setBankDeposit(b.getDeposit() + depositMoney)
}

func (b *Bank) takeMoneyfromDeposit(depositMoney float64) bool {
	// startMoney := b.getDeposit()
	if b.getDeposit() > depositMoney {
		b.setBankDeposit(b.getDeposit() - depositMoney)
		// fmt.Printf("\nDeposit Money bank: %.2f, and bank %v(start money: %.2f)\n", b.getDeposit(), b.getName(), startMoney)
		return true
	}
	return false
}

func (b *Bank) withdrawMoney(creditMoney float64) bool {
	if b.getCredit() > creditMoney {
		b.setBankCredit(b.getCredit() - creditMoney)
		return true
	}
	return false
}

func (c *Client) setName(name string) {
	c.Name = name
}

func (c *Client) setSurname(surname string) {
	c.Surname = surname
}

func (c *Client) setAccountNumber(accountNum int) {
	c.AccountNumber = accountNum
}
func (c *Client) setcDeposit(cdeposit float64) {
	c.cDeposit = cdeposit
}
func (c *Client) setcCredit(ccredit float64) {
	c.cCredit = ccredit
}

func (c *Client) withdrawMoneyFromDeposit(depositMoney float64, mutex *sync.Mutex) bool {
	mutex.Lock()
	startMoney := c.getcDeposit()
	if c.getcDeposit() < depositMoney {
		fmt.Printf("=========Client can't do that %d, withdraw money %.2f (start sum %.2f) and total sum is %.2f============\n",
			c.getAccountNumber(), depositMoney, startMoney, c.getcDeposit())
		mutex.Unlock()
		return false
	}
	if !banks[c.idBank].takeMoneyfromDeposit(depositMoney) {
		mutex.Unlock()
		return false
	}
	c.setcDeposit(c.getcDeposit() - depositMoney)
	fmt.Printf("Client: %d, withdraw money %.2f (start sum %.2f) and total sum is %.2f\n",
		c.getAccountNumber(), depositMoney, startMoney, c.getcDeposit())

	mutex.Unlock()
	return true
}

func (c *Client) putMoneyOnCreditAccount(creditMoney float64, mutex *sync.Mutex) bool {
	mutex.Lock()
	startMoney := c.getcCredit()
	totalMoney := c.getcCredit() + creditMoney
	if totalMoney < 0 {
		c.setcCredit(totalMoney)
		fmt.Printf("Client: %d, put money %.2f (start sum %.2f) and total sum is %.2f\n",
			c.getAccountNumber(), creditMoney, startMoney, c.getcCredit())
		mutex.Unlock()
		return true
	}
	fmt.Printf("=========Client can't do that %d, put money %.2f (start sum %.2f) and total sum is %.2f============\n",
		c.getAccountNumber(), creditMoney, startMoney, c.getcCredit())
	mutex.Unlock()
	return false
}

func main() {
	var mutexDeposit, mutexCredit sync.Mutex
	go func() {
		for {
			for _, bank := range banks {
				for _, c := range bank.getClients() {

					rand.Seed(time.Now().UnixNano())
					money := float64(rand.Intn(500))
					time.Sleep(10 * time.Millisecond)
					// fmt.Printf("Random %.2f for client %d\n", money, c.getAccountNumber())
					if c.getcCredit() == 0 {
						go c.withdrawMoneyFromDeposit(money, &mutexDeposit)
					} else {
						go c.putMoneyOnCreditAccount(money, &mutexCredit)
					}
				}
			}
			time.Sleep(time.Second * 8)
			// fmt.Println("Wait")
		}
	}()
	createMenu()
}

func showMenu() {
	fmt.Println("----Меню----")
	fmt.Println("1. Створення банку")
	fmt.Println("2. Створення клієнта для роботи з кредитами")
	fmt.Println("3. Створення клієнта для роботи з депозитами")
	fmt.Println("4. Виведення інформації про клієнта за прізвищем")
	fmt.Println("5. Виведення інформації про клієнта за номером рахунку")
	fmt.Println("6. Виведення інформації про Банки")
	fmt.Println("0. Завершення")
}

func createMenu() {
	for {
		showMenu()
		var x int
		fmt.Printf("Enter the value: \n")
		_, err := fmt.Scan(&x)
		fmt.Println("======================================")
		if err != nil {
			fmt.Println("Inccorect data!")
			continue
		}
		switch x {
		case 1:
			fmt.Println("=============================Creating a bank================================ ")

			Privat24 := newBank("Privat24", 4000, 10000, 10000, []*Client{})
			Oschad := newBank("Oschad", 6000, 10000, 10000, []*Client{})
			banks = append(banks, Privat24, Oschad)

			// banks = append(banks, createBank())
			printBanksAndClients()
		case 2:
			// fmt.Println("2")
			// fmt.Println("Work with credits!")
			// fmt.Println("Where do you want to create a client?")
			// isCreate, c := createClient(false)
			// if !isCreate {
			// 	fmt.Println("The client didn't create!")
			// 	break
			// }
			// banks[c.idBank].addClient(*c)
			// printBanksAndClients()
			banks[1].addClient(*newClient(1, "galya", "tursa", 111, 0, -1200))
			banks[1].addClient(*newClient(1, "galya", "tursa", 222, 0, -2400))
		case 3:
			// fmt.Println("Work with deposits!")
			// fmt.Println("Where do you want to create a client?")
			// isCreate, c := createClient(true)
			// if !isCreate {
			// 	fmt.Println("The client didn't create!")
			// 	break
			// }
			// banks[c.idBank].addClient(*c)
			// printBanksAndClients()
			banks[1].addClient(*newClient(1, "galya", "tursa", 333, 1200, 0))
			banks[1].addClient(*newClient(1, "galya", "tursa", 444, 2400, 0))
		case 4:
			fmt.Println("4")
			idBank := choosingTheBank()
			var name string
			fmt.Println("Enter the client name: ")
			fmt.Scan(&name)
			e, c := searchClientByName(name, idBank)
			if e {
				printClient(c)
			} else {
				fmt.Println("Not found a client!")
			}
		case 5:
			fmt.Println("5")
			idBank := choosingTheBank()
			var acc int
			fmt.Println("Enter the client account number: ")
			fmt.Scan(&acc)
			e, c := searchClientByAccount(acc, idBank)
			if e {
				printClient(c)
			} else {
				fmt.Println("Not found a client!")
			}
		case 6:
			printBanksAndClients()
		case 0:
			os.Exit(2)
		}
		fmt.Println("======================================")
	}
}

func printBanksAndClients() {
	for _, bank := range banks {
		fmt.Println("Bank", *bank)
		for _, client := range bank.Clients {
			fmt.Println("Client:", *client)
		}
	}
}

func createBank() *Bank {
	var (
		name      string
		bankMoney float64
		deposit   float64
		credit    float64
	)
	fmt.Println("Enter the name of Bank: ")
	fmt.Scan(&name)
	fmt.Println("Bank Money: ")
	fmt.Scan(&bankMoney)
	fmt.Println("Deposit: ")
	fmt.Scan(&deposit)
	fmt.Println("Credit: ")
	fmt.Scan(&credit)
	return newBank(name, bankMoney, deposit, credit, []*Client{})
}

func createClient(deposit bool) (bool, *Client) {
	var (
		idBank        int
		name          string
		surname       string
		accountNumber int
		cDeposit      float64
		cCredit       float64
	)
	idBank = choosingTheBank()
	fmt.Println("Enter the name: ")
	fmt.Scan(&name)
	fmt.Println("Enter the surname:")
	fmt.Scan(&surname)
	fmt.Println("Account number: ")
	fmt.Scan(&accountNumber)
	if deposit {
		fmt.Println("Deposit: ")
		fmt.Scan(&cDeposit)
		banks[idBank].putMoneyOnDeposit(cDeposit)
		cCredit = 0
	} else {
		fmt.Println("Credit: ")
		fmt.Scan(&cCredit)
		if !banks[idBank].withdrawMoney(cCredit) {
			return false, &Client{}
		}
		cDeposit = 0
	}
	return true, newClient(idBank, name, surname, accountNumber, cDeposit, -cCredit)
}

func printBanks() {
	for i, v := range banks {
		fmt.Printf("Num: %d, Name: %s\n", i, v.Name)
	}
}

func choosingTheBank() int {
	fmt.Println("================================")
	printBanks()
	fmt.Println("=====================================")
	var idBank int
	fmt.Println("Choose the bank: ")
	fmt.Scan(&idBank)
	return idBank
}

func searchClientByName(name string, idBank int) (isExist bool, c Client) {
	for _, v := range banks[idBank].Clients {
		if v.getSurname() == name {
			return true, *v
		}
	}
	return false, Client{}

}

func searchClientByAccount(account int, idBank int) (isExist bool, c Client) {
	for _, v := range banks[idBank].Clients {
		if v.getAccountNumber() == account {
			return true, *v
		}
	}
	return false, Client{}

}

func printClient(c Client) {
	fmt.Println("====================Client Info===============================")
	fmt.Printf("Client name: %s %s\n", c.getName(), c.getSurname())
	fmt.Printf("Account number: %d\n", c.getAccountNumber())
	fmt.Printf("Credit: %.2f\n", c.getcCredit())
	fmt.Printf("Deposit: %.2f\n", c.getcDeposit())
}

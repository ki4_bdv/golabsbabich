package main

import "fmt"

func main() {
	var chartype int8 = 'R'

	fmt.Printf("Code '%c' - %d\n", chartype, chartype)

	//Задание.
	//1. Вывести украинскую букву 'Ї'
	var letterI rune = 'Ї'

	fmt.Printf("Code '%c' - %d\n", letterI, letterI)
	//2. Пояснить назначение типа "rune"
	//rune 32-big integer число, яке зберігає номер символу в таблиці Unicode
	//Буква "Ї" - має 1031 номер в таблиці Unicode
}

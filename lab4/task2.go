package main

import (
	"github.com/andlabs/ui"
	_ "github.com/andlabs/ui/winmanifest"
	"strconv"
)
func FloatToString(inputNum float64) string {
	return strconv.FormatFloat(inputNum, 'f', 2, 64)
}
func initGUI2() {
	//Створення елементів форми
	window := ui.NewWindow("Відпочинок", 400, 175, false)
	entryDays := ui.NewEntry()
	entryCount := ui.NewEntry()
	labelDays := ui.NewLabel("Кількість днів")
	labelCount := ui.NewLabel("Кількість путівок")
	labelCountry := ui.NewLabel("Країна")
	comboCountry := ui.NewCombobox()
	labelSeason := ui.NewLabel("Пора року")
	comboSeason:= ui.NewCombobox()
	checkGuide := ui.NewCheckbox("Персональний гід")
	checkLuxury := ui.NewCheckbox("Номер люкс")
	buttonRes := ui.NewButton("Розрахувати")
	labelRes := ui.NewLabel("Вартість: ")
	labelRes.Hide()

	comboCountry.Append("Болгарія")
	comboCountry.Append("Німеччина")
	comboCountry.Append("Польща")
	comboSeason.Append("Зима")
	comboSeason.Append("Літо")
	comboCountry.SetSelected(0)
	comboSeason.SetSelected(0)

	box := ui.NewHorizontalBox()
	boxLeft := ui.NewVerticalBox()
	boxRight := ui.NewVerticalBox()
	//Заповнення боксів елементами
	boxLeft.Append(labelDays, false)
	boxLeft.Append(entryDays, false)
	boxLeft.Append(labelCount, false)
	boxLeft.Append(entryCount, false)
	boxRight.Append(labelCountry, false)
	boxRight.Append(comboCountry, false)
	boxRight.Append(labelSeason, false)
	boxRight.Append(comboSeason, false)
	boxLeft.Append(checkGuide, false)
	boxLeft.Append(checkLuxury, false)
	boxLeft.Append(buttonRes, false)
	boxLeft.Append(labelRes, false)
	box.Append(boxLeft, false)
	box.Append(boxRight, false)
	box.SetPadded(true)
	window.SetMargined(true)
	window.SetChild(box)
	//Обробник натискання кнопки---------------------------
	buttonRes.OnClicked(func(button *ui.Button) {
		days,errDays := strconv.Atoi(entryDays.Text())
		count,errCount := strconv.Atoi(entryCount.Text())
		if errDays == nil && errCount == nil {
			all := days * count
			price := 0.0
			selectedSeason := comboSeason.Selected()
			selectedCountry := comboCountry.Selected()
			switch selectedCountry {
			case 0:
				if selectedSeason == 1 {
					price += 100
				} else {
					price += 150
				}
			case 1:
				if selectedSeason == 1 {
					price += 160
				} else {
					price += 200
				}
			case 2:
				if selectedSeason == 1 {
					price += 120
				} else {
					price += 180
				}
			}
			if checkLuxury.Checked() {
				price += 0.2 * price
			}
			if checkGuide.Checked() {
				price += 50
			}
			labelRes.Show()
			labelRes.SetText("Вартість " + FloatToString(float64(all)*price) + " $")
		}else{
			labelRes.Show()
			labelRes.SetText("Помилка!")
			ui.MsgBoxError(window, "Помилка!", "Помилка при зчитуванні даних! Введіть коректні значення та повторіть спробу")
		}
	})

	window.OnClosing(func(*ui.Window) bool {
		ui.Quit()
		return true
	})
	window.Show()
}

func main() {
	err := ui.Main(initGUI2)
	if err != nil {
		panic(err)
	}
}
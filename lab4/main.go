package main

import (
	"github.com/andlabs/ui"
	_ "github.com/andlabs/ui/winmanifest"
	"strconv"
)

func initGUI() {
	window := ui.NewWindow("Склопакет", 300, 200, false)
	inputWidth := ui.NewEntry()
	inputHeight := ui.NewEntry()
	labelWidth := ui.NewLabel("Ширина (см): ")
	labelHeight := ui.NewLabel("Висота (см):   ")
	button := ui.NewButton("Розрахувати")
	comboBox := ui.NewCombobox()
	comboBox.Append("Дерево")
	comboBox.Append("Метал")
	comboBox.Append("Пластик")
	//HorizontalBox Width
	boxWidth := ui.NewHorizontalBox()
	boxWidth.SetPadded(true)
	boxWidth.Append(labelWidth, false)
	boxWidth.Append(inputWidth, false)
	//HorizontalBox Height
	boxHeight := ui.NewHorizontalBox()
	boxHeight.SetPadded(true)
	boxHeight.Append(labelHeight, false)
	boxHeight.Append(inputHeight, false)

	//HorizontalBox ComboBox
	boxMaterial := ui.NewHorizontalBox()
	boxMaterial.SetPadded(true)
	boxMaterial.Append(ui.NewLabel("Матеріал:       "), false)
	boxMaterial.Append(comboBox, false)
	//HorizontalBox ComboBox
	comboBoxType := ui.NewCombobox()
	comboBoxType.Append("Однокамерний")
	comboBoxType.Append("Двокамерний")
	checkBoxSill := ui.NewCheckbox("Підвіконня")
	result := ui.NewLabel("")
	box := ui.NewVerticalBox()
	box.Append(ui.NewLabel("Розміри вікна"), false)
	box.SetPadded(true)
	//box.Append(greeting, false)
	box.Append(boxWidth, false)
	box.Append(boxHeight, false)
	box.Append(boxMaterial, false)
	box.Append(ui.NewLabel("Склопакет"), false)
	box.Append(comboBoxType, false)
	box.Append(checkBoxSill, false)
	box.Append(button, false)
	box.Append(result, false)
	window.SetChild(box)
	button.OnClicked(func(*ui.Button) {
		price := 0.0
		area := 0.0
		height, errH := strconv.ParseFloat(inputHeight.Text(), 64)
		width, errW := strconv.ParseFloat(inputWidth.Text(), 64)
		if errH == nil && errW == nil {
			area = height * width
			flagBox := false
			n := comboBox.Selected()
			t := comboBoxType.Selected()
			if n == 0 && t == 0 {
				price = area * 0.25
			} else if n == 0 && t == 1 {
				price = area * 0.3
			} else if n == 1 && t == 0 {
				price = area * 0.05
			} else if n == 1 && t == 1 {
				price = area * 0.1
			} else if n == 2 && t == 0 {
				price = area * 0.15
			} else if n == 2 && t == 1 {
				price = area * 0.2
			} else {
				flagBox = true
			}
			checkBoxValue := checkBoxSill.Checked()
			if checkBoxValue {
				price += 35
			}
			if flagBox {
				result.SetText("Неправильний тип!")
			} else {
				result.SetText("Вартіcть: " + FloatToString(price) + " грн.")

			}

		} else {
			result.SetText("Помилка!")
		}

	})
	window.OnClosing(func(*ui.Window) bool {
		ui.Quit()
		return true
	})
	//boxLeft.SetPadded(true)
	window.SetMargined(true)
	window.Show()
}
func main() {
	err := ui.Main(initGUI)
	if err != nil {
		panic(err)
	}
}

func FloatToString(inputNum float64) string {
	// to convert a float number to a string
	return strconv.FormatFloat(inputNum, 'f', 3, 64)
}

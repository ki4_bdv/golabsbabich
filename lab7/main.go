package main

import (
	"fmt"
	"math"
	"net/http"
	"strconv"
)

const (
	pageHeader = `<!DOCTYPE html>
	<html lang="en">
	
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Document</title>
		<style>
			body {
				font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
				font-size: 20px;
			}
	
			form input {
				padding: 5px;
			}
	
			table {
				padding: 5px;
				border: 1px solid black;
				margin-bottom: 20px;
			}
	
			.btn-submit {
				padding: 5px 20px;
				font-size: 20px;
				margin-bottom: 20px;
				background-color: rgb(157, 173, 177);
				border: 2px solid rgb(52, 19, 196);
			}
	
			.result {
				border-collapse: collapse;
				margin-top: 20px;
			}
	
			.result td {
				margin-top: 20px;
				text-align: center;
				padding: 5px;
				font-size: 20px;
				border: 2px solid black;
			}
	
			.error {
				color: red;
			}
		</style>
	</head>`
	pageBody = `<body>
   <h1>Quadratic equation</h1>
   <form action="/" method="GET">
      <table>
         <tr>
            <td>Enter the vlalue of a:</td>
            <td><input type="text" name="a"></td>
         </tr>
         <tr>
            <td>Enter the vlalue of b:</td>
            <td><input type="text" name="b"></td>
         </tr>
         <tr>
            <td>Enter the vlalue of c:</td>
            <td><input type="text" name="c"></td>
         </tr>
      </table>
      <input class="btn-submit" type="submit" value="Solve">
   </form>`
	pageFooter = `</body></html>`
	result1    = `<table class="result">
	<tr>
		<td>Result: x<sub>1</sub></td>
		<td>%.2f</td>
	</tr>
</table>`
	result2 = `<table class="result">
<tr>
	<td>Result: x<sub>1</sub></td>
	<td>%.2f</td>
</tr>
<tr>
	<td>Result: x<sub>2</sub></td>
	<td>%.2f</td>
</tr>
</table>`
	p       = `<p>%s</p>`
	anError = `<p class="error">
	%s</p>`
)

func main() {
	http.HandleFunc("/", func(rw http.ResponseWriter, r *http.Request) {
		fmt.Fprint(rw, pageHeader, pageBody)
		var a, b, c float64
		var err1, err2, err3 error
		if r.Method == "POST" {
			err := r.ParseForm()
			post := r.PostForm
			if err != nil {
				fmt.Fprintf(rw, anError, err)
				return
			}
			a, err1 = strconv.ParseFloat(post.Get("a"), 10)
			b, err2 = strconv.ParseFloat(post.Get("b"), 10)
			c, err3 = strconv.ParseFloat(post.Get("c"), 10)
		}
		if r.Method == "GET" {
			a, err1 = strconv.ParseFloat(r.URL.Query().Get("a"), 10)
			b, err2 = strconv.ParseFloat(r.URL.Query().Get("b"), 10)
			c, err3 = strconv.ParseFloat(r.URL.Query().Get("c"), 10)

		}
		if err1 != nil || err2 != nil || err3 != nil {
			fmt.Fprintf(rw, anError, "Error!")
			return
		}
		d := b*b - 4*a*c
		if d < 0 {
			fmt.Fprintf(rw, p, "This equation doesn't have a solving!")
		} else if d > 0 {
			x1 := (-b + math.Sqrt(d)) / (2 * a)
			x2 := (-b - math.Sqrt(d)) / (2 * a)
			fmt.Fprintf(rw, result2, x1, x2)
		} else {
			x := -b / (2 * a)
			fmt.Fprintf(rw, result1, x)
		}
		fmt.Fprint(rw, "\n", pageFooter)
	})

	http.ListenAndServe(":80", nil)
}

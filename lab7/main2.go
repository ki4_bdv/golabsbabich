package main

import (
	"errors"
	"fmt"
	"math"
	"net/http"
	"strconv"
	"strings"
)

const (
	pageHeader = `<!DOCTYPE html>
	<html lang="en">
	
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Document</title>
		<style>
			* {
				box-sizing: border-box;
			}
	
			body {
				font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
				font-size: 20px;
	
			}
	
			form input[type="text"] {
				padding: 5px;
				width: 800px;
			}
	
			table {
				padding: 5px;
				border: 1px solid black;
				margin-bottom: 20px;
			}
	
			.btn-submit {
				padding: 5px 20px;
				font-size: 20px;
				background-color: rgb(157, 173, 177);
				border: 2px solid rgb(52, 19, 196);
			}
	
			.result {
				border-collapse: collapse;
				margin-top: 20px;
			}
	
			.result td {
				margin-top: 20px;
				text-align: center;
				padding: 5px;
				font-size: 20px;
				border: 2px solid black;
			}
	
			.error {
				color: red;
			}
		</style>
	</head>`
	pageFooter = `</body></html>`
	pageBody   = `<body>
   <h1>Task 2</h1>
   <form action="/" method="POST">
      <table>
         <tr>
            <td>Enter the numbers example (5 7 2 1): </td>
            <td><input type="text" name="array"></td>
         </tr>
      </table>
      <input class="btn-submit" type="submit" value="Solve">
   </form>
   <table class="result">

   </table>
</body>`
	result = ` <table class="result">
	<tr>
		<td>Sum of positive numbers: </td>
		<td>%d</td>
	</tr>
	<tr>
		<td>Multiply numbers between min and max abs elements: </td>
		<td>%d</td>
	</tr>
</table>`
	anError = `<p class="error">
	%s</p>`
)

func main() {
	http.HandleFunc("/", func(rw http.ResponseWriter, r *http.Request) {
		fmt.Fprint(rw, pageHeader, pageBody)
		var slice []int
		var array string
		if r.Method == "POST" {
			err := r.ParseForm()
			post := r.PostForm
			if err != nil {
				fmt.Fprintf(rw, anError, err)
				return
			}
			array = post.Get("array")
		}
		if r.Method == "GET" {
			array = r.URL.Query().Get("array")
		}
		var err error
		sliceStr := strings.Split(array, " ")
		slice, err = convertStringToIntSlice(sliceStr)
		if err != nil {
			fmt.Fprintf(rw, anError, err.Error())
			fmt.Fprint(rw, "\n", pageFooter)
			return
		}
		fmt.Fprintf(rw, result, countPositive(slice), multiplyNumbersBetweenMaxAndMinAbs(slice))
		fmt.Fprint(rw, "\n", pageFooter)
	})

	http.ListenAndServe(":80", nil)
}

func countPositive(arr []int) int {
	result := 0
	for _, v := range arr {
		if v > 0 {
			result += v
		}
	}
	return result
}

func convertStringToIntSlice(array []string) ([]int, error) {
	var result []int
	for _, v := range array {
		intValue, err := strconv.ParseInt(v, 10, 32)
		if err != nil {
			return result, errors.New("Error")
		}
		result = append(result, int(intValue))
	}
	return result, nil
}

func multiplyNumbersBetweenMaxAndMinAbs(arr []int) int {
	result := 1
	minValue := math.MaxInt32
	maxValue := math.MinInt32
	minIndex := 0
	maxIndex := 0

	for i, v := range arr {
		v = int(math.Abs(float64(v)))
		if v < minValue {
			minValue = v
			minIndex = i
		}
		if v > maxValue {
			maxValue = v
			maxIndex = i
		}
	}

	var from, to int

	if minIndex < maxIndex {
		from = minIndex + 1
		to = maxIndex
	} else {
		from = maxIndex + 1
		to = minIndex
	}
	for i := from; i < to; i++ {
		result *= arr[i]
	}
	// fmt.Println("minValue: ", minValue, "maxValue", maxValue, "minIndex", minIndex, "maxIndex", maxIndex)
	return result
}

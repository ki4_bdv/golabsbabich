~package main

import (
	"net/http"
	"strconv"
	"text/template"
	"time"
)

const (
	index = "main.tpl"
	place = "place.tpl"
)

type Place struct {
	Name        string
	Src         string
	Href        string
	Description string
}
type Index struct {
	Title  string
	Places []*Place
	Name   string
	Year   string
}
type PlaceInfo struct {
	Name         string
	Area         string
	Region       string
	Src          string
	Src2         string
	IsPopulation bool
	Population   string
	Location     string
	Author       string
	Year         string
	Climat       string
}

var year string = strconv.Itoa(time.Now().Year())

func main() {

	http.HandleFunc("/", func(rw http.ResponseWriter, r *http.Request) {
		myVars := Index{}
		tmpl, err := template.ParseFiles(index)
		templates := template.Must(tmpl, err)
		myVars.Title = "Найкращі міста України"
		myVars.Name = "Бабіч Дмитро"
		myVars.Year = year
		myVars.Places = []*Place{
			{
				"Джарилгач",
				"https://petropavlivka.city/upload/article/o_1ed41lubfar81dvs1hfl7ob4i11r.jpeg",
				"/page1/",
				"Цей острів на Херсонщині все ще залишається найбільшим заповітним безлюдним островом у Європі. А фотографії звідси можна сміливо видавати за знімки з райських іноземних куточків: така сама лазурна вода та білосніжний пісок. На острові можна виділити дві умовні туристичні зони – Глибока бухта та маяки. Їх тут два – новий, що працює, та старий. Останній, за легендою, спроєктував сам Ейфель.",
			},
			{
				"Рожеве озеро",
				"https://petropavlivka.city/upload/article/o_1ecniumj81poflkm12un59lltm77.jpg",
				"/page2/",
				"Воно дійсно рожеве: що тепліше надворі, то насиченіший колір. Озеро має таке забарвлення через мікроводорость, яка живе в дуже солоній воді. Сюди насамперед варто приїжджати під час заходу сонця, вечірнє світло найкраще передає красу.",
			},
			{
				"Станіслав-Аджигольські маяки",
				"https://petropavlivka.city/upload/article/o_1ecnj1isd8rqmjhek1qsp19aj8c.jpg",
				"/page3/",
				"Їх два: задній та передній. Задній Станіслав-Аджигольський – найвищий маяк в Україні та один із двадцяти найвищих у світі. Він знаходиться поблизу  села Рибальче Голопристанського району, у південнно-східному куті Дніпровського лиману. Його висота сягає 76 м.  Передній розташований поблизу острова Вербка проти села Станіслав. Його висота 34 м.",
			},
			{
				"Озеро Сиваш",
				"https://petropavlivka.city/upload/article/o_1ecnj66iguf31ojp12visp2nr1aq.jpg",
				"/page4/",
				"Це українське «мертве», а точніше – «гниле море». Це затока Азовського моря, що розділена на одинадцять мілких водойм. Вода непридатна для живих організмів через надмірну солоність. Тут живе понад сто видів птахів. Багато з них занесені до Червоної книги України.",
			},
			// {
			// 	"",
			// 	"",
			// 	"",
			// 	"",
			// },
		}
		templates.ExecuteTemplate(rw, index, myVars)
	})
	http.HandleFunc("/page1/", func(rw http.ResponseWriter, r *http.Request) {
		myVars := PlaceInfo{}
		tmpl, err := template.ParseFiles(place)
		templates := template.Must(tmpl, err)
		myVars.Name = "Джарилгач"
		myVars.Area = "56"
		myVars.Region = "Херсонська область"
		myVars.Src = "https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/Djarylgac.jpg/375px-Djarylgac.jpg"
		myVars.Src2 = "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/%D0%A1%D1%82%D0%B5%D0%BF%D0%BE%D0%B2%D0%B0_%D1%87%D0%B0%D1%81%D1%82%D0%B8%D0%BD%D0%B0%2C_%D0%BE._%D0%94%D0%B6%D0%B0%D1%80%D0%B8%D0%BB%D0%B3%D0%B0%D1%87.jpg/390px-%D0%A1%D1%82%D0%B5%D0%BF%D0%BE%D0%B2%D0%B0_%D1%87%D0%B0%D1%81%D1%82%D0%B8%D0%BD%D0%B0%2C_%D0%BE._%D0%94%D0%B6%D0%B0%D1%80%D0%B8%D0%BB%D0%B3%D0%B0%D1%87.jpg"
		myVars.IsPopulation = true
		myVars.Population = "Безлюдний"
		myVars.Location = "Знаходиться у Каркінітській затоці Чорного моря, відділений від узбережжя материка Джарилгацькою затокою. Острів має видовжену форму із заходу на схід, на заході продовжується довгою піщаною косою, що «з'єднує» острів з берегом. Косу острова від суходолу відділяє вузька протока, котра інколи пересихає, формально перетворюючи острів на півострів. За 2 км від коси, на узбережжі Херсонщини, знаходиться курортне селище Лазурне. Близько 7 км Джарилгацької затоки відділяють острів від міста Скадовськ, що знаходиться на протилежному березі."
		myVars.Author = "Бабіч Дмитро"
		myVars.Climat = "помірно континентальний"
		myVars.Year = year
		templates.ExecuteTemplate(rw, place, myVars)
	})
	http.HandleFunc("/page2/", func(rw http.ResponseWriter, r *http.Request) {
		myVars := PlaceInfo{}
		tmpl, err := template.ParseFiles(place)
		templates := template.Must(tmpl, err)
		myVars.Name = "Рожеве озеро"
		myVars.Area = "122"
		myVars.Region = "Херсонська область"
		myVars.Src = "https://static.ukrinform.com/photos/2018_03/1521030800-6372.jpg"
		myVars.Src2 = "https://amazing-ukraine.com/wp-content/uploads/2020/07/%D0%91%D0%B5%D0%B7-%D0%BD%D0%B0%D0%B7%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F-79-768x511.jpg"
		myVars.IsPopulation = false
		myVars.Location = "Рожевий колір водойми зумовлений дією одноклітинних водоростей Дуналіелла солоноводна, які під дією сонця виробляють бетакаротин. Чим спекотнішим видається літо, тим більше води випаровується з озера й тим насиченішого кольору набуває ропа. Відступаючи, вода залишає на березі тонни білого соляного «піску». Місцями соляні кристали збиваються, утворюючи справжні «сталаґміти». Білосніжний берег та рожеве озеро справляють незабутнє враження, перетворюючи цю частину Херсонщини на один з наймальовничіших та найзагадковіших куточків України."
		myVars.Author = "Бабіч Дмитро"
		myVars.Climat = "помірно континентальний"
		myVars.Year = year
		templates.ExecuteTemplate(rw, place, myVars)
	})
	http.HandleFunc("/page4/", func(rw http.ResponseWriter, r *http.Request) {
		myVars := PlaceInfo{}
		tmpl, err := template.ParseFiles(place)
		templates := template.Must(tmpl, err)
		myVars.Name = "Озеро Сиваш"
		myVars.Area = "2560"
		myVars.Region = "Херсонська область"
		myVars.Src = "https://static.ukrinform.com/photos/2018_03/1521030800-6372.jpg"
		myVars.Src2 = "https://vasilev-life.ru/wp-content/uploads/%D0%9E%D0%B7%D0%B5%D1%80%D0%BE-%D0%A1%D0%B8%D0%B2%D0%B0%D1%88-%D0%B2-%D0%9A%D1%80%D1%8B%D0%BC%D1%83.jpg"
		myVars.IsPopulation = false
		myVars.Location = "Сиваш — система дрібних мілководних (максимальна глибина до 3,5 м) заток на західному березі Азовського моря, між Херсонською областю та Кримом. Відокремлений від основної частини Азовського моря піщаною косою Арабатська стрілка. Довжина — близько 200 км. Ширина — 2-35 км. Пересічна площа — нестала, 2 400 км², з яких бл. 100 км² припадає на острови і 560 км² на ділянки, які лише періодично вкриті водою. Солоність води Сивашу сягає 260 ‰. Солі — хлористі сполуки Na, Mg, сульфат Mg — важлива хімічна сировина."
		myVars.Author = "Бабіч Дмитро"
		myVars.Climat = "помірно континентальний"
		myVars.Year = year
		templates.ExecuteTemplate(rw, place, myVars)
	})
	http.HandleFunc("/page3/", func(rw http.ResponseWriter, r *http.Request) {
		myVars := PlaceInfo{}
		tmpl, err := template.ParseFiles(place)
		templates := template.Must(tmpl, err)
		myVars.Name = "Озеро Сиваш"
		myVars.Area = "2"
		myVars.Region = "Херсонська область"
		myVars.Src = "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1f/%D0%A1%D1%82%D0%B0%D0%BD%D1%96%D1%81%D0%BB%D0%B0%D0%B2-%D0%90%D0%B4%D0%B6%D0%B8%D0%B3%D0%BE%D0%BB%D1%8C%D1%81%D1%8C%D0%BA%D1%96_%D0%BC%D0%B0%D1%8F%D0%BA%D0%B8_%D0%B7%D0%B0%D0%B4%D0%BD%D1%96%D0%B93.jpg/1200px-%D0%A1%D1%82%D0%B0%D0%BD%D1%96%D1%81%D0%BB%D0%B0%D0%B2-%D0%90%D0%B4%D0%B6%D0%B8%D0%B3%D0%BE%D0%BB%D1%8C%D1%81%D1%8C%D0%BA%D1%96_%D0%BC%D0%B0%D1%8F%D0%BA%D0%B8_%D0%B7%D0%B0%D0%B4%D0%BD%D1%96%D0%B93.jpg"
		myVars.Src2 = "https://visitkherson.gov.ua/wp-content/uploads/2020/06/uz.advisor.travel-zadnij-stanislav-adzhigolskij-majak-01-min-1.jpg"
		myVars.IsPopulation = false
		myVars.Location = "Назва маяка походить від Мису Станіслав та мису Аджиголь, що знаходиться на іншому березі лиману. Створи маяків направлені як раз в бік мису Аджиголь. Гирло Дніпра вважається складним місцем для навігації. При впадінні в лиман річка перетворюється на хитросплетіння численних рукавів, островів-плавнів і вузьких проток між ними (єриків)."
		myVars.Author = "Бабіч Дмитро"
		myVars.Climat = "помірно континентальний"
		myVars.Year = year
		templates.ExecuteTemplate(rw, place, myVars)
	})
	http.ListenAndServe(":80", nil)
}

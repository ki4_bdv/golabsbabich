<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Document</title>
   <link rel="stylesheet" href="css/style.css">
   <style>
      *,
      *::after,
      *::before {
         margin: 0;
         padding: 0;
         border: 0;
         -webkit-box-sizing: border-box;
         box-sizing: border-box;
      }

      body {
         background-color: #333;
         font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
         font-size: 16px;
         color: #fff;
      }

      .yellow {
         color: #e7e700;
      }

      .header {
         margin: 20px 0;
         padding: 10px 0;
         font-size: 24px;
         line-height: 150%;
         letter-spacing: 2px;
      }

      .wrapper {
         overflow: hidden;
         display: -webkit-box;
         display: -ms-flexbox;
         display: flex;
         -webkit-box-orient: vertical;
         -webkit-box-direction: normal;
         -ms-flex-direction: column;
         flex-direction: column;
         min-height: 100vh;
      }

      .container {
         max-width: 1200px;
         padding: 0 15px;
         margin: 0 auto;
      }

      .main {
         -webkit-box-flex: 1;
         -ms-flex-positive: 1;
         flex-grow: 1;
      }

      .menu {
         list-style-type: none;
         background-color: black;
      }

      .menu__item {
         color: #58abb9;
      }

      .button {
         padding: 15px;
         font-size: 16px;
         text-decoration: none;
         color: white;
         letter-spacing: 2px;
         font-weight: 700;
         cursor: pointer;
         text-align: center;
         border: 2px solid white;
         background-color: #e6b61b;
         -webkit-transition: background linear 0.3s;
         transition: background linear 0.3s;
      }

      .button:hover {
         background-color: #c29d25;
      }

      .place {
         display: -webkit-box;
         display: -ms-flexbox;
         display: flex;
         -webkit-box-orient: vertical;
         -webkit-box-direction: normal;
         -ms-flex-direction: column;
         flex-direction: column;
         margin-bottom: 30px;
      }

      .place__name {
         margin: 20px 0;
         font-size: 20px;
         line-height: 150%;
         letter-spacing: 2px;
      }

      .item {
         display: -webkit-box;
         display: -ms-flexbox;
         display: flex;
         min-height: 250px;
      }

      .item__leftside {
         display: -webkit-box;
         display: -ms-flexbox;
         display: flex;
         -webkit-box-orient: vertical;
         -webkit-box-direction: normal;
         -ms-flex-direction: column;
         flex-direction: column;
         -webkit-box-flex: 0;
         -ms-flex: 0 0 33.333%;
         flex: 0 0 33.333%;
      }

      .item__photo {
         -webkit-box-flex: 1;
         -ms-flex-positive: 1;
         flex-grow: 1;
         position: relative;
         overflow: hidden;
         margin-bottom: 20px;
      }

      .item__title {
         text-align: center;
         letter-spacing: 2px;
         margin-bottom: 10px;

      }

      .item__photo img {
         -o-object-fit: cover;
         object-fit: cover;
         width: 100%;
         height: 100%;
         position: absolute;
         top: 0;
         left: 0;
      }

      .item__description {
         padding: 0 10px 0;
         line-height: 1.5;
      }

      @media screen and (max-width: 997px) {
         .item__description {
            font-size: 12px;
         }
      }

      .footer {
         margin: 20px 0;
      }

      .footer__text {
         text-align: center;
         font-size: 16px;
         line-height: 1.5;
         letter-spacing: 2px;
      }

      .information {
         margin-bottom: 30px;
      }

      .information__title {
         text-align: center;
         margin: 20px 0;
      }

      .information__inner {
         display: -webkit-box;
         display: -ms-flexbox;
         display: flex;
         min-height: 200px;
      }

      .information__photo {
         -webkit-box-flex: 1;
         -ms-flex: 1 1 50%;
         flex: 1 1 50%;
         position: relative;
         order: 1;
      }

      .information__photo img {
         object-fit: cover;
         position: absolute;
         width: 100%;
         height: 100%;
         top: 0;
         left: 0;
      }

      .information__info {
         order: -1;
         line-height: 150%;
         -webkit-box-flex: 1;
         -ms-flex: 1 1 50%;
         flex: 1 1 50%;
         padding: 10px;
      }
   </style>
</head>

<body>
   <div class="wrapper">
      <div class="main">
         <header class="header">
            <div class="container">
               <span class="yellow">{{ .Name }}</span>
            </div>
         </header>

         <div class="information">
            <div class="container">
               <h2 class="information__title">Додаткова інформація</h2>
               <div class="information__inner">
                  <div class="information__photo">
                     <img src="{{ .Src2 }}" alt="">
                  </div>
                  <div class="information__info">
                     <div>Площа: {{ .Area }} км<sup>2</sup></div>
                     <div>Регіон: {{ .Region }}</div>
                     {{if .IsPopulation}}
                     <div>Населення: {{ .Population }}</div>
                     {{ end }}
                     <div>Клімат: {{ .Climat }}</div>
                  </div>
               </div>
            </div>
         </div>
         <div class="container">
            <div class="place">
               <div class="item">
                  <div class="item__leftside">
                     <div class="item__photo">
                        <img src={{ .Src }} alt="" />
                     </div>
                     <a class="button" href="/">Повернутися</a>
                  </div>
                  <div class="item__description">
                     <h2 class="item__title">Місце розатшування</h2>
                     {{ .Location }}
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="footer">
         <div class="container">
            <div class="footer__text">
               © Copyright, {{ .Author}}, {{ .Year}}
            </div>
         </div>
      </div>
   </div>
</body>

</html>
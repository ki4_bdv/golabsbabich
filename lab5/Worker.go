package main

import (
	"fmt"
	"math"
	"os"
	"strconv"
	"time"
	"unicode"
)

type Company struct {
	Name     string
	Position string
	Salary   float64
}

//Constructor Company
func makeCompany(name, position string, salary float64) Company {
	return Company{name, position, salary}
}

//Getters Company
func CompanyName(c Company) string {
	return c.Name
}
func Position(c Company) string {
	return c.Position
}
func Salary(c Company) float64 {
	return c.Salary
}

//Setters Company
func SetCompanyName(c Company, name string) {
	c.Name = name
}

func SetPosition(c Company, position string) {
	c.Position = position
}
func SetSalary(c Company, salary float64) {
	if c.Salary > 0 {
		c.Salary = salary
	}
}

type Worker struct {
	Name      string
	Year      int
	Month     int
	WorkPlace Company
}

//Constructor Worker
func makeWorker(name string, year, month int, companyName, position string, salary float64) Worker {
	return Worker{name, year, month, makeCompany(companyName, position, salary)}
}
func Name(worker Worker) string {
	return worker.Name
}
func Year(worker Worker) int {
	return worker.Year
}

func Month(worker Worker) int {
	return worker.Month
}
func WorkPlace(worker Worker) Company {
	return worker.WorkPlace
}

func SetName(worker Worker, name string) {
	worker.Name = name
}
func SetYear(worker Worker, year int) {
	worker.Year = year
}
func SetMonth(worker Worker, month int) {
	if worker.Month >= 1 && worker.Month <= 12{
		worker.Month = month
	} else {
		worker.Month = 1
	}
}
func SetWorkPlace(worker Worker, companyName, position string, salary float64) {
	SetPosition(worker.WorkPlace, position)
	SetCompanyName(worker.WorkPlace, companyName)
	SetSalary(worker.WorkPlace, salary)
}

func main() {
	//worker := makeWorker("John", 2021, 1, "Amazon", "director", 1300)
	//fmt.Println(worker)
	//fmt.Println(GetWorkPosition(worker))
	//fmt.Println(GetWorkExperience(worker))
	//fmt.Println(GetTotalMoney(worker))
	workers := ReadWorkersArray()
	PrintWorkers(workers)
	min, max := GetWorkersInfo(workers)
	fmt.Println(min, max)
}

func GetWorkPosition(w Worker) string {
	return Position(w.WorkPlace)
}
func GetWorkExperience(w Worker) int {
	l, err := time.LoadLocation("Europe/Vienna")
	if err != nil {
		panic(err)
	}
	startYear := Year(w)
	startMonth := Month(w)
	oldDate := time.Date(startYear, time.Month(startMonth), 1, 0, 0, 0, 0, l)
	dateDiff := oldDate.Sub(time.Now())
	return int(math.Abs(float64(int(dateDiff) / 1000000000 / 3600 / 24 / 30)))
}

func GetTotalMoney(w Worker) float64 {
	return float64(GetWorkExperience(w)) * Salary(w.WorkPlace)
}

func ReadWorkersArray() []Worker {
	var count int
	var result []Worker
	count = checkNumbers("Введіть кількість записів: ", isInt)
	var (
		name        string
		year        int
		month       int
		companyName string
		position    string
		salary      float64
	)
	for i := 0; i < count; i++ {
		fmt.Println("Введіть ім'я: ")
		fmt.Fscan(os.Stdin, &name)
		//checkNumber("Введіть рік початку роботи: ", &year)
		year = checkNumbers("Введіть рік початку роботи(xxxx): ", isYear)
		month = checkNumbers("Введіть місяць початку роботи(числом): ", isMonth)
		fmt.Println("Введіть назва компанії: ")
		fmt.Fscan(os.Stdin, &companyName)
		fmt.Println("Введіть посаду працівника: ")
		fmt.Fscan(os.Stdin, &position)
		value := "dfsd"
		for !isFloat64(value) {
			fmt.Println("Введіть зарплатню працівника:")
			fmt.Fscan(os.Stdin, &value)
			salary, _ = strconv.ParseFloat(value, 64)
		}
		result = append(result, makeWorker(name, year, month, companyName, position, salary))
		fmt.Println("===========================================")
	}
	return result
}

func PrintWorker(w Worker) {
	fmt.Println("Name: ", Name(w))
	fmt.Println("Year: ", Year(w))
	fmt.Println("Month: ", Month(w))
	fmt.Println("Company Name: ", CompanyName(w.WorkPlace))
	fmt.Println("Position: ", Position(w.WorkPlace))
	fmt.Println("Salary: ", Salary(w.WorkPlace))
	fmt.Println("=========================================")
}

func PrintWorkers(w []Worker) {
	for _, worker := range w {
		PrintWorker(worker)
	}
}

func GetWorkersInfo(w []Worker) (float64, float64) {
	min := math.MaxFloat64
	max := -1000000.0
	for _, worker := range w {
		sal := Salary(worker.WorkPlace)
		if sal > max {
			max = sal
		}
		if sal < min {
			min = sal
		}
	}
	return min, max
}

func isInt(s string) bool {
	for _, c := range s {
		if !unicode.IsDigit(c) {
			return false
		}
	}
	return true
}
func isYear(s string) bool {
	n, _ := strconv.Atoi(s)
	return isInt(s) && (n >= 1900 && n <= 2021)
}
func isMonth(s string) bool {
	n, _ := strconv.Atoi(s)
	return isInt(s) && (n >= 1 && n <= 12)
}
func isFloat64(s string) bool {
	_, err := strconv.ParseFloat(s, 64)
	if err != nil {
		return false
	}
	return true
}

func checkNumbers(message string, criteria func(string) bool) int {
	isNumber := false
	result := 0
	str := ""
	for !isNumber {
		fmt.Println(message)
		fmt.Fscan(os.Stdin, &str)
		if criteria(str) {
			isNumber = true
			result, _ = strconv.Atoi(str)
		} else {
			fmt.Println("Неправильне введення!")
		}
	}
	return result
}

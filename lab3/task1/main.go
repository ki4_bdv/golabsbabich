package main

import (
	"fmt"
	"math"
)

func main() {
	generator := lcg()
	hash := map[int]int{}
	//Кількість випадкових значень
	totalCount := 5
	//Hash
	for i := 0; i < totalCount; i++ {
		key := generator() % 10
		if _, ok := hash[key]; ok {
			hash[key]++
		} else {
			hash[key] = 1
		}
	}
	//////////////////////////////////////
	averageMap := getP(hash, totalCount)
	for i, k := range averageMap {
		fmt.Printf("x(%d) -------- p(%d) = %.2f\n", i, i, k)
	}
	/////Математичне сподівання
	expectation := getMathExpectation(averageMap)
	fmt.Printf("Математичне сподівання: %.2f\n", expectation)
	/////Диперсія
	dispersion := getDispersion(averageMap, expectation)
	fmt.Printf("Диперсія: %.2f\n", dispersion)
	/////Числова характеристика
	numberCharacteristic := getNumericalCharacteristic(dispersion)
	fmt.Printf("Числова характеристика: %.2f\n", numberCharacteristic)

}

func lcg() func() int {
	const (
		a = 1103515245
		m = 2147483648
		c = 12345
	)
	seed := 1000
	return func() int {
		seed = (seed*a + c) % m
		return seed
	}
}

func getP(hash map[int]int, totalCount int) map[int]float64 {
	newMap := map[int]float64{}
	for k, v := range hash {
		newMap[k] = float64(v) / float64(totalCount)
	}
	return newMap
}

func getMathExpectation(m map[int]float64) float64 {
	sum := 0.0
	for k, v := range m {
		sum += float64(k) * v
	}
	return sum
}

func getDispersion(m map[int]float64, expectation float64) float64{
	sum := 0.0
	for k, v := range m {
		sum += math.Pow(float64(k) - expectation, 2.0) * v
	}
	return sum
}

func getNumericalCharacteristic(dispersion float64) float64{
	return math.Sqrt(dispersion)
}

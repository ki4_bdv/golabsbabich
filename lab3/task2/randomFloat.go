package main

import (
	"fmt"
	"math"
)

func main() {
	arr := generate(1664525, 1013904223, int(math.Pow(2, 32)), 1000, 0, 150, 2000, 4)
	fmt.Println(arr)
}

func lcg(a, c, m, seed int) func() int {
	return func() int {
		seed = (seed*a + c) % m
		return seed
	}
}

func generate(a, c, m, seed, start, end, count, precision int) []float64 {
	precision = int(math.Pow(10, float64(precision)))
	start = start * precision
	end = end * precision
	randomNumbers := make([]float64, count)
	generator := lcg(a, c, m, seed)
	for i, _ := range randomNumbers {
		randomNumbers[i] = float64((generator()%(end-start+1))+start) / float64(precision)
	}
	return randomNumbers
}
